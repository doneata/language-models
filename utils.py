from itertools import islice

import numpy as np


def calculate_perplexity(log_probs):
    return np.exp(- np.sum(log_probs) / len(log_probs))


def sliding_window(seq, n):
    "Returns a sliding window (of width n) over data from the iterable"
    it = iter(seq)
    result = tuple(islice(it, n))
    if len(result) == n:
        yield result
    for elem in it:
        result = result[1:] + (elem,)
        yield result
