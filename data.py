import os
import torch

from collections import Counter


BASE_PATH = '/home/doneata/work/language-models'


class Vocab(object):

    def __init__(self, words, voc_size):
        self.unknown = '<unk>'
        self.counter = Counter(words)
        self.total_words = sum(self.counter.values())
        self.word_to_index = {w: i for i, (w, _) in enumerate(self.counter.most_common(voc_size))}
        self.index_to_word = {i: w for w, i in self.word_to_index.items()}
        self.add_word(self.unknown)

    def add_word(self, word):
        if word not in self.word_to_index:
            index = len(self)
            self.word_to_index[word] = index
            self.index_to_word[index] = word
        self.counter[word] += 1

    def encode(self, word):
        try:
            return self.word_to_index[word]
        except KeyError:
            return self.word_to_index[self.unknown]

    def decode(self, index):
        return self.index_to_word[index]

    def __len__(self):
        return len(self.word_to_index)


class Dictionary(object):
    def __init__(self):
        self.word2idx = {}
        self.idx2word = []

    def add_word(self, word):
        if word not in self.word2idx:
            self.idx2word.append(word)
            self.word2idx[word] = len(self.idx2word) - 1
        return self.word2idx[word]

    def __len__(self):
        return len(self.idx2word)


class Corpus(object):
    def __init__(self, path):
        self.dictionary = Dictionary()
        self.train = self.tokenize(os.path.join(path, 'train.txt'))
        self.valid = self.tokenize(os.path.join(path, 'valid.txt'))
        self.test = self.tokenize(os.path.join(path, 'test.txt'))

    def tokenize(self, path):
        """Tokenizes a text file."""
        assert os.path.exists(path), "Missing file {}".format(path)
        # Add words to the dictionary
        with open(path, 'r') as f:
            tokens = 0
            for line in f:
                words = line.split() + ['<eos>']
                tokens += len(words)
                for word in words:
                    self.dictionary.add_word(word)

        # Tokenize file content
        with open(path, 'r') as f:
            ids = torch.LongTensor(tokens)
            token = 0
            for line in f:
                words = line.split() + ['<eos>']
                for word in words:
                    ids[token] = self.dictionary.word2idx[word]
                    token += 1

        return ids


def load_ptb(split):
    path = os.path.join(BASE_PATH, 'cs224d-a2/data/ptb/ptb.{}.txt'.format(split))
    with open(path, 'r') as f:
        for line in f.readlines():
            for word in line.split():
                yield word
            yield '<eos>'


def load_news():
    pass


def prepare_data(loader, voc_size):
    vocab = Vocab(loader('train'), voc_size)
    datasets = {split: map(vocab.encode, loader(split)) for split in ('train', 'valid', 'test')}
    return vocab, datasets


LOADERS = {
    'ptb': load_ptb,
    'news': load_news,
}
