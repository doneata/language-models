import pdb
import random

from itertools import (
    tee,
)

import numpy as np

SEED = 1337
random.seed(SEED)
np.random.seed(SEED)

from keras import backend as K
from keras.callbacks import (
    Callback,
    ModelCheckpoint,
    ReduceLROnPlateau,
)
from keras.layers import (
    Input,
    Dense,
    Dropout,
    Flatten,
    TimeDistributed,
)
from keras.layers.embeddings import Embedding
from keras.layers.recurrent import (
    GRU,
    LSTM,
)
from keras.models import (
    Model,
    load_model,
)
from keras.preprocessing.sequence import pad_sequences
from keras.optimizers import Adam
from keras.utils import (
    to_categorical,
)

from data import (
    LOADERS,
    prepare_data,
)

from utils import (
    sliding_window,
)


class ResetRecurrentStates(Callback):
    def __init__(self, reset_proba):
        self.reset_proba = reset_proba

    def on_batch_end(self, batch, logs={}):
        if random.random() < self.reset_proba:
            self.model.reset_states()


def prepare_data_single_output(data, nr_steps):
    """Runs sliding window and maps each window to a single output:
        (w1, w2, w3) -> w4
        (w2, w3, w4) -> w5
        (w3, w4, w5) -> w6
    """
    windows = sliding_window(data, n=nr_steps + 1)
    slided_data = np.vstack(window for window in windows)
    x = slided_data[:, :-1]
    y = slided_data[:,  -1]
    return x, y


def data_generator_multiple_output(data, nr_steps, batch_size, nr_classes, skip_step):
    i = 0
    n = len(data)
    data = np.array(data)
    while True:
        m = nr_steps * batch_size
        if i + m >= n:
            i = 0
        x = data[i    : i + m    ].reshape(-1, nr_steps)
        y = data[i + 1: i + m + 1].reshape(-1, nr_steps)
        y = to_categorical(y, nr_classes)
        yield x, y
        i += skip_step()


def prepare_data_multiple_output(data, nr_steps, batch_size):
    data = np.array(list(data))
    m = nr_steps * batch_size
    n = len(data) // m * m + 1
    x = data[0: n - 1].reshape(-1, nr_steps)
    y = data[1: n    ].reshape(-1, nr_steps)
    return x, y

## Models

def create_model_white(input_size, vocab_size, embedding_size=64):
    inputs = Input(shape=(input_size, ))
    x = Embedding(vocab_size, embedding_size)(inputs)
    x = Flatten()(x)
    outputs = Dense(vocab_size, activation='softmax')(x) 
    return Model(inputs=inputs, outputs=outputs)


def create_model_apricot(input_size, vocab_size, batch_size, embedding_size=256, gru_size=512, stateful=False):
    inputs = Input(shape=(input_size, ), batch_shape=(None, input_size))
    x = Embedding(vocab_size, embedding_size)(inputs)
    x = LSTM(gru_size, stateful=stateful, return_sequences=True, dropout=0.3, recurrent_dropout=0.3)(x)
    x = LSTM(gru_size, stateful=stateful, return_sequences=True, dropout=0.3, recurrent_dropout=0.3)(x)
    x = Dropout(0.7)(x)
    outputs = TimeDistributed(Dense(vocab_size, activation="softmax"))(x)
    return Model(inputs=inputs, outputs=outputs)


def create_model_almond(input_size, vocab_size, batch_size, embedding_size=64, gru_size=64, stateful=False):
    # Architecture inspired by the following implementation:
    # https://github.com/roemmele/keras-rnn-notebooks/blob/master/language_modeling/language_modeling.ipynb
    inputs = Input(shape=(input_size, ), batch_shape=(None, input_size))
    x = Embedding(vocab_size, embedding_size)(inputs)
    # x = GRU(gru_size, stateful=stateful, return_sequences=True)(x)
    x = GRU(gru_size, stateful=stateful, return_sequences=False, dropout=0.3, recurrent_dropout=0.3)(x)
    outputs = Dense(vocab_size, activation="softmax")(x)
    return Model(inputs=inputs, outputs=outputs)


MODELS = {
    'white': create_model_white,
    'almond': create_model_almond,
    'apricot': create_model_apricot,
}

PREPARE_DATA = {
    'single-output': prepare_data_single_output,
    'multiple-output': prepare_data_multiple_output,
}


class Config:
    # Parameter choices taken from:
    # "On the state of the art of evaluation in neural language models"
    # https://arxiv.org/abs/1707.05589
    BATCH_SIZE = 64
    NR_EPOCHS = 1500
    NR_STEPS = 35


class Config2:
    BATCH_SIZE = 32
    NR_EPOCHS = 50
    NR_STEPS = 64


C = Config()
VOCAB_SIZE = 10000
MODEL_PATH = 'apricot.h5'


def find_learning_rate():
    pass


def main():
    vocab, datasets = prepare_data(LOADERS['ptb'], VOCAB_SIZE)

    skip_step = lambda: int(random.uniform(C.NR_STEPS // 2, 2 * C.NR_STEPS))
    tr_generator = data_generator_multiple_output(list(datasets['train']), C.NR_STEPS, C.BATCH_SIZE, VOCAB_SIZE, skip_step)

    va_x, va_y = prepare_data_multiple_output(datasets['valid'], C.NR_STEPS, C.BATCH_SIZE)
    va_y = to_categorical(va_y, VOCAB_SIZE)

    if False:
        model = create_model_apricot(C.NR_STEPS, VOCAB_SIZE, C.BATCH_SIZE, stateful=False)
        model.compile(
            loss="categorical_crossentropy",
            optimizer=Adam(beta_1=0.0, beta_2=0.999),
        )
    else:
        model = load_model(MODEL_PATH)

    checkpointer = ModelCheckpoint(filepath=MODEL_PATH, verbose=1, save_best_only=True)
    reduce_learning_rate = ReduceLROnPlateau(monitor='val_loss', factor=0.1, patience=30, min_lr=0)
    # reset_recurrent_states = ResetRecurrentStates(reset_proba=0.01)
    model.fit_generator(
        generator=tr_generator,
        epochs=C.NR_EPOCHS,
        steps_per_epoch=100,
        validation_data=(va_x, va_y),
        shuffle=False,
        callbacks=[checkpointer, reduce_learning_rate],
        initial_epoch=1000,
    )

    # val_generator, val_generator_2 = tee(val_generator)
    # model.reset_states()
    # print(model.evaluate_generator(val_generator, val_steps))

    model.reset_states()
    model.save(MODEL_PATH)


def try_model():
    model = load_model(MODEL_PATH)
    vocab, datasets = prepare_data(LOADERS['ptb'], VOCAB_SIZE)
    te_x, te_y = prepare_data_multiple_output(datasets['train'], C.NR_STEPS, C.BATCH_SIZE)
    for i in range(100):
        pred = model.predict(te_x[i: i + 1])
        print(' '.join(vocab.index_to_word[index] for index in te_x[i]))
        print(' '.join(vocab.index_to_word[p.argmax()] for p in pred[0]))
        # print(vocab.index_to_word[te_y[i, -1]])
        print()
        pdb.set_trace()


if __name__ == '__main__':
    # try_model()
    main()
